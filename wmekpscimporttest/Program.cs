﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WMEKModel;
using Microsoft.Extensions.Configuration;
using System.Data;
using ReadWriteCsv;
using System.Globalization;

namespace wmekpscimporttest
{
    class Program
    {
        public static IConfigurationRoot Configuration { get; set; }

        static void Main(string[] args)
        {
            string contentRootPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\"));
            string currentDirectory = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, ".\\"));
            string appSettingsFileName = contentRootPath + "appsettings.json";
            if (!File.Exists(appSettingsFileName))
            {
                appSettingsFileName = currentDirectory + "appsettings.json";
                if (!File.Exists(appSettingsFileName))
                {
                    Console.WriteLine("Datei {0} nicht vorhanden", appSettingsFileName);
                    return;
                }
                else
                {
                    Console.WriteLine("Datei {0} gefunden", appSettingsFileName);
                    var builder = new ConfigurationBuilder()
                        .SetBasePath(currentDirectory)
                        .AddJsonFile("appsettings.json");
                    Configuration = builder.Build();
                }
            }
            else
            {
                Console.WriteLine("Datei {0} gefunden", appSettingsFileName);
                var builder = new ConfigurationBuilder()
                    .SetBasePath(contentRootPath)
                    .AddJsonFile("appsettings.json");
                Configuration = builder.Build();
            }

            // Import directory
            string importPath = string.Empty;
            try
            {
                var importSection = Configuration.GetSection("ImportDirectories");
                //importPath = importSection.GetChildren().Where(c => c.Key == "LocalTest").FirstOrDefault().Value;
                importPath = importSection.GetChildren().Where(c => c.Key == "Production").FirstOrDefault().Value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Quell-Verzeichnis in appsettings.json nicht gefunden. Ausnahme: {0}", e.Message);
                return;
            }
            Console.WriteLine("Quell-Verzeichnis: {0}", importPath);
            // Done directory
            string donePath = string.Empty;
            try
            {
                var doneSection = Configuration.GetSection("DoneDirectories");
                //donePath = doneSection.GetChildren().Where(c => c.Key == "LocalTest").FirstOrDefault().Value;
                donePath = doneSection.GetChildren().Where(c => c.Key == "Production").FirstOrDefault().Value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Done-Verzeichnis in appsettings.json nicht gefunden. Ausnahme: {0}", e.Message);
                return;
            }
            Console.WriteLine("Verzeichnis für verarbeitete Dateien: {0}", donePath);

            using (var db = new EKDBContext(Configuration.GetConnectionString("DefaultConnection")))
            {
               
                string sSearchPattern = "zbwst_kokrs_*";
                string[] arrFiles = Directory.GetFiles(importPath, sSearchPattern);
             
                // CDHDR
                sSearchPattern = "zbwst_cdhdr_fi_*";
                arrFiles = Directory.GetFiles(importPath, sSearchPattern);
                if (arrFiles.Length > 0)
                {
                    for (int i = 0; i < arrFiles.Length; i++)
                    {
                        Console.WriteLine("Import Buchhaltungsbelege . . . . .  Datei {0} ", arrFiles[i]);
                        List<CDHDR> listCDHDR = UpdateCDHDR(db, arrFiles[i]);
                        Console.WriteLine("  Anzahl Datensätze . . . . . . . . . . {0} \n", listCDHDR.Count);
                        MoveFile(arrFiles[i], importPath, donePath);
                    }
                }
                // CDPOS
                sSearchPattern = "zbwst_cdpos_fi_*";
                arrFiles = Directory.GetFiles(importPath, sSearchPattern);
                if (arrFiles.Length > 0)
                {
                    for (int i = 0; i < arrFiles.Length; i++)
                    {
                        Console.WriteLine("Import Buchhaltungsbelege Positionen Datei {0} ", arrFiles[i]);
                        List<CDPOS> listCDPOS = UpdateCDPOS(db, arrFiles[i]);
                        Console.WriteLine("  Anzahl Datensätze . . . . . . . . . . {0} \n", listCDPOS.Count);
                        MoveFile(arrFiles[i], importPath, donePath);
                    }
                }
                // Kreditoren
                sSearchPattern = "zbwst_kred_*";
                arrFiles = Directory.GetFiles(importPath, sSearchPattern);
                for (int i = 0; i < arrFiles.Length; i++)
                {
                    Console.WriteLine("Import Kreditoren (Stammdaten). . Datei {0} ", arrFiles[i]);
                    List<EK_KRED> listKRED = UpdateKRED(db, arrFiles[i]);
                    Console.WriteLine("  Anzahl Datensätze . . . . . . . . . . {0} \n", listKRED.Count);
                    MoveFile(arrFiles[i], importPath, donePath);
                }

                // BKPF
                sSearchPattern = "zbwst_bkpf_*";
                arrFiles = Directory.GetFiles(importPath, sSearchPattern);
                if (arrFiles.Length > 0)
                {
                    for (int i = 0; i < arrFiles.Length; i++)
                    {
                        Console.WriteLine("Import Buchhaltungsbelege . . . . .  Datei{0} ", arrFiles[i]);
                        List<BKPF> listBKPF = UpdateBKPF(db, arrFiles[i]);
                        Console.WriteLine("  Anzahl Datensätze . . . . . . . . . . {0} \n", listBKPF.Count);
                        MoveFile(arrFiles[i], importPath, donePath);
                    }


                }

               
                // BSEG
                sSearchPattern = "zbwst_bseg_*";
                arrFiles = Directory.GetFiles(importPath, sSearchPattern);
                if (arrFiles.Length > 0)
                {
                    for (int i = 0; i < arrFiles.Length; i++)
                    {
                        Console.WriteLine("Import Buchhaltungsbelege Positionen Datei {0} ", arrFiles[i]);
                        List<BSEG> listBSEG = UpdateBSEG(db, arrFiles[i]);
                        Console.WriteLine("  Anzahl Datensätze . . . . . . . . . . {0} \n", listBSEG.Count);
                        MoveFile(arrFiles[i], importPath, donePath);
                    }
                }
               
  /*
                //RBKP
                sSearchPattern = "zbwst_rbkp_*";
                arrFiles = Directory.GetFiles(importPath, sSearchPattern);
                if (arrFiles.Length > 0)
                {
                    for (int i = 0; i < arrFiles.Length; i++)
                    {
                        Console.WriteLine("Import Buchhaltungsbelege Positionen Datei {0} ", arrFiles[i]);
                        List<RBKP> listRBKP = UpdateRBKP(db, arrFiles[i]);
                        Console.WriteLine("  Anzahl Datensätze . . . . . . . . . . {0} \n", listRBKP.Count);
                        MoveFile(arrFiles[i], importPath, donePath);
                    }
                }

*/

            }

        }


        public static List<BKPF> UpdateBKPF(EKDBContext db, string filename)
        {
            DateTime now = DateTime.Now;
            List<BKPF> list = GetListBKPF(filename);
            AdminDataImport adi = new AdminDataImport();
            adi.ImportFileName = filename;
            adi.ImportDateTime = now;
            adi.TargetTable = "tblBKPF";
            adi.RowsInFile = list.Count;

            foreach (BKPF bkpf in list)
            {
                BKPF bkpfDB = db.tblBKPF.Where(e => e.COMP_KEY == bkpf.COMP_KEY).FirstOrDefault();
                int? bukrs_id = null;
                if (bkpfDB == null || (bkpfDB != null && !bkpfDB.Equals(bkpf)))
                {
                    EK_BUKRS bukDB = db.tblBUKRS.Where(b => b.BUKRS == bkpf.BUKRS).FirstOrDefault();
                    if (bukDB == null)
                    {
                        throw new Exception("Buchungskreis " + bkpf.BUKRS + " nicht vorhanden");
                    }
                    bukrs_id = bukDB.ID;
                    //Bukr;LIFNR,EBELN,
                    if (bkpfDB == null)
                    {
                        db.tblBKPF.Add(new BKPF()
                        {
                            COMP_KEY = bkpf.COMP_KEY,
                            BUKRS = bkpf.BUKRS,
                            BUKRS_ID = (int)bukrs_id,
                            BELNR = bkpf.BELNR,
                            GJAHR = bkpf.GJAHR,
                            BLART = bkpf.BLART,
                            BLDAT = bkpf.BLDAT,
                            BLDAT_DT = bkpf.BLDAT_DT,
                            BUDAT = bkpf.BUDAT,
                            BUDAT_DT = bkpf.BUDAT_DT,
                            CPUDT = bkpf.CPUDT,
                            CPUDT_DT = bkpf.CPUDT_DT,
                            CPUTM = bkpf.CPUTM,
                            AEDAT = bkpf.AEDAT,
                            AEDAT_DT = bkpf.AEDAT_DT,
                            UPDDT = bkpf.UPDDT,
                            UPDDT_DT = bkpf.UPDDT_DT,
                            USNAM = bkpf.USNAM,
                            STBLG = bkpf.STBLG,
                            STJAH = bkpf.STJAH,
                            BSTAT = bkpf.BSTAT,
                            WAERS = bkpf.WAERS,
                            CreationDateTime = now
                        });
                        adi.RowsInserted++;
                    }
                    else
                    {
                        if (!bkpfDB.Equals(bkpf))
                        {
                            bkpfDB.COMP_KEY = bkpf.COMP_KEY;
                            bkpfDB.BUKRS = bkpf.BUKRS;
                            bkpfDB.BUKRS_ID = (int)bukrs_id;
                            bkpfDB.BELNR = bkpf.BELNR;
                            bkpfDB.GJAHR = bkpf.GJAHR;
                            bkpfDB.BLART = bkpf.BLART;
                            bkpfDB.BLDAT = bkpf.BLDAT;
                            bkpfDB.BLDAT_DT = bkpf.BLDAT_DT;
                            bkpfDB.BUDAT = bkpf.BUDAT;
                            bkpfDB.BUDAT_DT = bkpf.BUDAT_DT;
                            bkpfDB.CPUDT = bkpf.CPUDT;
                            bkpfDB.CPUDT_DT = bkpf.CPUDT_DT;
                            bkpfDB.CPUTM = bkpf.CPUTM;
                            bkpfDB.AEDAT = bkpf.AEDAT;
                            bkpfDB.AEDAT_DT = bkpf.AEDAT_DT;
                            bkpfDB.UPDDT = bkpf.UPDDT;
                            bkpfDB.UPDDT_DT = bkpf.UPDDT_DT;
                            bkpfDB.USNAM = bkpf.USNAM;
                            bkpfDB.STBLG = bkpf.STBLG;
                            bkpfDB.STJAH = bkpf.STJAH;
                            bkpfDB.BSTAT = bkpf.BSTAT;
                            bkpfDB.WAERS = bkpf.WAERS;
                            bkpfDB.UpdateDateTime = DateTime.Now;
                            db.tblBKPF.Update(bkpfDB);
                            adi.RowsUpdated++;
                        }
                    }

                }
            }

            db.tblAdminDataImport.Add(adi);
            db.SaveChanges();

            return list;
        }
        public static List<BKPF> GetListBKPF(string filename)
        {
            List<BKPF> list = new List<BKPF>();
            CsvFile file = new CsvFile(filename);
            DataTable dt = file.GetDataTable();
            string[] colNames = file.GetColumnNames();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                BKPF bkpf = GetBKPF(colNames, dr);
                cnt++;
                if (bkpf.COMP_KEY.Length > 0)
                {
                    list.Add(bkpf);
                }
            }
            return list;
        }
        public static BKPF GetBKPF(string[] colNames, DataRow dr)
        {
            BKPF bkpf = new BKPF();
            bkpf.BUKRS = dr[colNames[0]].ToString().Trim();
            bkpf.BELNR = dr[colNames[1]].ToString().Trim();
            bkpf.GJAHR = dr[colNames[2]].ToString().Trim();
            bkpf.BLART = dr[colNames[3]].ToString().Trim();
            bkpf.BLDAT = dr[colNames[4]].ToString().Trim();
            bkpf.BLDAT_DT = ConvertSAPDate(bkpf.BLDAT);
            bkpf.BUDAT = dr[colNames[5]].ToString().Trim();
            bkpf.BUDAT_DT = ConvertSAPDate(bkpf.BUDAT);
            bkpf.CPUDT = dr[colNames[6]].ToString().Trim();
            bkpf.CPUTM = dr[colNames[7]].ToString().Trim();
            bkpf.CPUDT_DT = ConvertSAPDateTime(bkpf.CPUDT, bkpf.CPUTM);
            bkpf.AEDAT = dr[colNames[8]].ToString().Trim();
            bkpf.AEDAT_DT = ConvertSAPDate(bkpf.AEDAT);
            bkpf.UPDDT = dr[colNames[9]].ToString().Trim();
            bkpf.UPDDT_DT = ConvertSAPDate(bkpf.UPDDT);
            bkpf.USNAM = dr[colNames[10]].ToString().Trim();
            bkpf.STBLG = dr[colNames[11]].ToString().Trim();
            bkpf.STJAH = dr[colNames[12]].ToString().Trim();
            bkpf.BSTAT = dr[colNames[13]].ToString().Trim();
            bkpf.COMP_KEY = dr[colNames[14]].ToString().Trim();
            bkpf.WAERS = dr[colNames[15]].ToString().Trim();
            return bkpf;
        }




        public static List<BSEG> UpdateBSEG(EKDBContext db, string filename)
        {
            DateTime now = DateTime.Now;
            List<BSEG> list = GetListBSEG(filename);
            AdminDataImport adi = new AdminDataImport();
            adi.ImportFileName = filename;
            adi.ImportDateTime = now;
            adi.TargetTable = "tblBSEG";
            adi.RowsInFile = list.Count;

            foreach (BSEG bseg in list)
            {
                BSEG bsegDB = db.tblBSEG.Where(e => e.COMP_KEY == bseg.COMP_KEY).FirstOrDefault();

                if (bsegDB == null || (bsegDB != null && !bsegDB.Equals(bseg)))
                {
                    EK_BUKRS bukDB = db.tblBUKRS.Where(b => b.BUKRS == bseg.BUKRS).FirstOrDefault();
                    if (bukDB == null)
                    {
                        throw new Exception("Buchungskreis " + bseg.BUKRS + " nicht vorhanden");
                    }

                    EK_KRED kredDB = db.tblKRED.Where(k => k.LIFNR == bseg.LIFNR).FirstOrDefault();
                    if (kredDB == null)
                    {
                        throw new Exception("Kreditor " + bseg.LIFNR + " nicht vorhanden");
                    }

                    EKKO ekkoDB = db.tblEKKO.Where(e => e.EBELN != null).FirstOrDefault();
                    if (ekkoDB == null)
                    {
                        throw new Exception("EK-Beleg " + bseg.EBELN + " nicht vorhanden");
                    }

                    string comp_keyv = bseg.COMP_KEY.Substring(0, 21);
                    BKPF bkpfDB = db.tblBKPF.Where(b => b.COMP_KEY == comp_keyv).FirstOrDefault();


                    if (bkpfDB == null)
                    {
                        throw new Exception("Beleg " + bseg.COMP_KEY + " nicht vorhanden");
                    }

                    if (bsegDB == null)
                    {
                        db.tblBSEG.Add(new BSEG()
                        {
                            COMP_KEY = bseg.COMP_KEY,
                            BUKRS = bseg.BUKRS,
                            BUKRS_ID = bukDB.ID,
                            BELNR = bseg.BELNR,
                            BELNR_ID = bkpfDB.ID,
                            GJAHR = bseg.GJAHR,
                            BUZEI = bseg.BUZEI,
                            LIFNR = bseg.LIFNR,
                            LIFNR_ID = kredDB.ID,
                            ZUONR = bseg.ZUONR,
                            AUGDT = bseg.AUGDT,
                            AUGDT_DT = bseg.AUGDT_DT,
                            AUGCP = bseg.AUGCP,
                            AUGCP_DT = bseg.AUGCP_DT,
                            AUGBL = bseg.AUGBL,
                            BSCHL = bseg.BSCHL,
                            GSBER = bseg.GSBER,
                            ZFBDT = bseg.ZFBDT,
                            ZFBDT_DT = bseg.ZFBDT_DT,
                            ZTERM = bseg.ZTERM,
                            ZBD1T = bseg.ZBD1T,
                            ZBD2T = bseg.ZBD2T,
                            ZBD3T = bseg.ZBD3T,
                            ZBD1P = bseg.ZBD1P,
                            ZBD2P = bseg.ZBD2P,
                            ZLSPR = bseg.ZLSPR,
                            ZBFIX = bseg.ZBFIX,
                            MSCHL = bseg.MSCHL,
                            MANSP = bseg.MSCHL,
                            MADAT = bseg.MADAT,
                            MADAT_DT = bseg.MADAT_DT,
                            MANST = bseg.MANST,
                            MABER = bseg.MABER,
                            EBELN = bseg.EBELN,
                            EBELN_ID = ekkoDB.ID,
                            EBELP = bseg.EBELP,
                            SPGRP = bseg.SPGRP,
                            SPGRM = bseg.SPGRM,
                            SPGRT = bseg.SPGRT,
                            SPGRG = bseg.SPGRG,
                            SPGRV = bseg.SPGRV,
                            SPGRQ = bseg.SPGRQ,
                            UMSKZ = bseg.UMSKZ,
                            SHKZG = bseg.SHKZG,
                            MWSKZ = bseg.MWSKZ,
                            DMBTR = bseg.DMBTR,
                            WRBTR = bseg.WRBTR,
                            MWSTS = bseg.MWSTS,
                            WMWST = bseg.WMWST,
                            HWBAS = bseg.HWBAS,
                            FWBAS = bseg.FWBAS,
                            SKFBT = bseg.SKFBT,
                            SKNTO = bseg.SKNTO,
                            WSKTO = bseg.WSKTO,
                            ZLSCH = bseg.ZLSCH,
                            BVTYP = bseg.BVTYP,
                            CreationDateTime = now
                        });
                        adi.RowsInserted++;
                    }
                    else
                    {
                        if (!bsegDB.Equals(bseg))
                        {
                            bsegDB.COMP_KEY = bseg.COMP_KEY;
                            bsegDB.BUKRS = bseg.BUKRS;
                            bsegDB.BUKRS_ID = bukDB.ID;
                            bsegDB.BELNR = bseg.BELNR;
                            bsegDB.BELNR_ID = bkpfDB.ID;
                            bsegDB.GJAHR = bseg.GJAHR;
                            bsegDB.BUZEI = bseg.BUZEI;
                            bsegDB.LIFNR = bseg.LIFNR;
                            bsegDB.LIFNR_ID = kredDB.ID;
                            bsegDB.ZUONR = bseg.ZUONR;
                            bsegDB.AUGDT = bseg.AUGDT;
                            bsegDB.AUGDT_DT = bseg.AUGDT_DT;
                            bsegDB.AUGCP = bseg.AUGCP;
                            bsegDB.AUGCP_DT = bseg.AUGCP_DT;
                            bsegDB.AUGBL = bseg.AUGBL;
                            bsegDB.BSCHL = bseg.BSCHL;
                            bsegDB.GSBER = bseg.GSBER;
                            bsegDB.ZFBDT = bseg.ZFBDT;
                            bsegDB.ZFBDT_DT = bseg.ZFBDT_DT;
                            bsegDB.ZTERM = bseg.ZTERM;
                            bsegDB.ZBD1T = bseg.ZBD1T;
                            bsegDB.ZBD2T = bseg.ZBD2T;
                            bsegDB.ZBD3T = bseg.ZBD3T;
                            bsegDB.ZBD1P = bseg.ZBD1P;
                            bsegDB.ZBD2P = bseg.ZBD2P;
                            bsegDB.ZLSPR = bseg.ZLSPR;
                            bsegDB.ZBFIX = bseg.ZBFIX;
                            bsegDB.MSCHL = bseg.MSCHL;
                            bsegDB.MANSP = bseg.MSCHL;
                            bsegDB.MADAT = bseg.MADAT;
                            bsegDB.MADAT_DT = bseg.MADAT_DT;
                            bsegDB.MANST = bseg.MANST;
                            bsegDB.MABER = bseg.MABER;
                            bsegDB.EBELN = bseg.EBELN;
                            bsegDB.EBELN_ID = ekkoDB.ID;
                            bsegDB.EBELP = bseg.EBELP;
                            bsegDB.SPGRP = bseg.SPGRP;
                            bsegDB.SPGRM = bseg.SPGRM;
                            bsegDB.SPGRT = bseg.SPGRT;
                            bsegDB.SPGRG = bseg.SPGRG;
                            bsegDB.SPGRV = bseg.SPGRV;
                            bsegDB.SPGRQ = bseg.SPGRQ;
                            bsegDB.UMSKZ = bseg.UMSKZ;
                            bsegDB.SHKZG = bseg.SHKZG;
                            bsegDB.MWSKZ = bseg.MWSKZ;
                            bsegDB.DMBTR = bseg.DMBTR;
                            bsegDB.WRBTR = bseg.WRBTR;
                            bsegDB.MWSTS = bseg.MWSTS;
                            bsegDB.WMWST = bseg.WMWST;
                            bsegDB.HWBAS = bseg.HWBAS;
                            bsegDB.FWBAS = bseg.FWBAS;
                            bsegDB.SKFBT = bseg.SKFBT;
                            bsegDB.SKNTO = bseg.SKNTO;
                            bsegDB.WSKTO = bseg.WSKTO;
                            bsegDB.ZLSCH = bseg.ZLSCH;
                            bsegDB.BVTYP = bseg.BVTYP;

                            bsegDB.UpdateDateTime = DateTime.Now;
                            db.tblBSEG.Update(bsegDB);
                            adi.RowsUpdated++;
                        }
                    }

                }
            }
            db.tblAdminDataImport.Add(adi);
            db.SaveChanges();

            return list;
        }
        public static List<BSEG> GetListBSEG(string filename)
        {
            List<BSEG> list = new List<BSEG>();
            CsvFile file = new CsvFile(filename);
            DataTable dt = file.GetDataTable();
            string[] colNames = file.GetColumnNames();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                BSEG bseg = GetBSEG(colNames, dr);
                cnt++;
                if (bseg.COMP_KEY.Length > 0)
                {
                    list.Add(bseg);
                }
            }
            return list;
        }
        public static BSEG GetBSEG(string[] colNames, DataRow dr)
        {
            BSEG bseg = new BSEG();
            bseg.BUKRS = dr[colNames[0]].ToString().Trim();
            bseg.BELNR = dr[colNames[1]].ToString().Trim();           
            bseg.GJAHR = dr[colNames[2]].ToString().Trim();
            bseg.BUZEI = dr[colNames[3]].ToString().Trim();
            bseg.AUGDT = dr[colNames[4]].ToString().Trim();
            bseg.AUGDT_DT = ConvertSAPDate(bseg.AUGDT);
            bseg.AUGCP = dr[colNames[5]].ToString().Trim();
            bseg.AUGCP_DT = ConvertSAPDate(bseg.AUGDT);
            bseg.AUGBL = dr[colNames[6]].ToString().Trim();
            bseg.BSCHL = dr[colNames[7]].ToString().Trim();
            bseg.GSBER = dr[colNames[8]].ToString().Trim();
            bseg.ZFBDT = dr[colNames[9]].ToString().Trim();
            bseg.ZFBDT_DT = ConvertSAPDate(bseg.ZFBDT);
            bseg.ZTERM = dr[colNames[10]].ToString().Trim();
            bseg.ZBD1T = Int32.Parse(dr[colNames[11]].ToString().Trim());
            bseg.ZBD2T = Int32.Parse(dr[colNames[12]].ToString().Trim());
            bseg.ZBD3T = Int32.Parse(dr[colNames[13]].ToString().Trim());
            bseg.ZBD1P = Decimal.Parse(dr[colNames[14]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.ZBD2P = Decimal.Parse(dr[colNames[15]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.ZLSPR = dr[colNames[16]].ToString().Trim();
            bseg.ZBFIX = dr[colNames[17]].ToString().Trim();
            bseg.MSCHL = dr[colNames[18]].ToString().Trim();
            bseg.MANSP = dr[colNames[19]].ToString().Trim();
            bseg.MADAT = dr[colNames[20]].ToString().Trim();
            bseg.MADAT_DT = ConvertSAPDate(bseg.MADAT);
            bseg.MANST = dr[colNames[21]].ToString().Trim();
            bseg.MABER = dr[colNames[22]].ToString().Trim();
            bseg.EBELN = dr[colNames[23]].ToString().Trim();
            bseg.EBELP = dr[colNames[24]].ToString().Trim();
            bseg.SPGRP = dr[colNames[25]].ToString().Trim();
            bseg.SPGRM = dr[colNames[26]].ToString().Trim();
            bseg.SPGRT = dr[colNames[27]].ToString().Trim();
            bseg.SPGRG = dr[colNames[28]].ToString().Trim();
            bseg.LIFNR = dr[colNames[29]].ToString().Trim();
            bseg.ZUONR = dr[colNames[30]].ToString().Trim();
            bseg.COMP_KEY = dr[colNames[31]].ToString().Trim();
            bseg.SPGRV = dr[colNames[32]].ToString().Trim();
            bseg.SPGRQ = dr[colNames[33]].ToString().Trim();
            bseg.UMSKZ = dr[colNames[34]].ToString().Trim();
            bseg.SHKZG = dr[colNames[35]].ToString().Trim();
            bseg.MWSKZ = dr[colNames[36]].ToString().Trim();
            bseg.DMBTR = Decimal.Parse(dr[colNames[37]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.WRBTR = Decimal.Parse(dr[colNames[38]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.MWSTS = Decimal.Parse(dr[colNames[39]].ToString().Trim(), CultureInfo.InvariantCulture);            
            bseg.WMWST = Decimal.Parse(dr[colNames[40]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.HWBAS = Decimal.Parse(dr[colNames[41]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.FWBAS = Decimal.Parse(dr[colNames[42]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.SKFBT = Decimal.Parse(dr[colNames[43]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.SKNTO = Decimal.Parse(dr[colNames[44]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.WSKTO = Decimal.Parse(dr[colNames[45]].ToString().Trim(), CultureInfo.InvariantCulture);
            bseg.ZLSCH = dr[colNames[46]].ToString().Trim();
            bseg.BVTYP = dr[colNames[47]].ToString().Trim();

            return bseg;
        }

        public static void MoveFile(string fullFileName, string importPath, string donePath)
        {
            string now = DateTimeOffset.Now.ToString("yyyyMMddHHmmss");
            string fileName = fullFileName.Replace(importPath, string.Empty);
            string fileDone = Path.Combine(donePath, $"{now}_{fileName}");

            File.Move(fullFileName, fileDone);
            return;
        }
        static public DateTime? ConvertSAPDate(string DATUM)
        {
            DateTime? dt = null;
            if (DATUM.Length == 8 && !DATUM.Equals("00000000"))
            {
                int year = Int32.Parse(DATUM.Substring(0, 4));
                int month = Int32.Parse(DATUM.Substring(4, 2));
                int day = Int32.Parse(DATUM.Substring(6, 2));
                if (year < 2000)
                {
                    year = 2000;
                    month = 1;
                    day = 1;
                }
                try
                {
                    dt = new DateTime(year, month, day);
                }
                catch (Exception)
                {
                    dt = null;
                }
            }
            return dt;
        }
        static public DateTime ConvertSAPDateTime(string DATUM, string UZEIT)
        {
            DateTime dt = DateTime.Now;
            if (DATUM.Length == 8 && !DATUM.Equals("00000000"))
            {
                int year = Int32.Parse(DATUM.Substring(0, 4));
                int month = Int32.Parse(DATUM.Substring(4, 2));
                int day = Int32.Parse(DATUM.Substring(6, 2));
                int hour = Int32.Parse(UZEIT.Substring(0, 2));
                int minute = Int32.Parse(UZEIT.Substring(2, 2));
                int second = Int32.Parse(UZEIT.Substring(4, 2));
                try
                {
                    dt = new DateTime(year, month, day, hour, minute, second);
                }
                catch (Exception)
                {
                    try
                    {
                        dt = new DateTime(year, month, day);
                    }
                    catch (Exception)
                    {
                        dt = new DateTime(2000, 1, 1);
                        throw;
                    }
                }
            }
            return dt;
        }


        public static List<CDHDR> UpdateCDHDR(EKDBContext db, string filename)
        {
            DateTime now = DateTime.Now;
            List<CDHDR> list = GetListCDHDR(filename);

            AdminDataImport adi = new AdminDataImport();
            adi.ImportFileName = filename;
            adi.ImportDateTime = now;
            adi.TargetTable = "tblCDHDR";
            adi.RowsInFile = list.Count;


            foreach (CDHDR cd in list)
            {
                db.tblCDHDR.Add(new CDHDR()
                {
                    OBJECTCLAS = cd.OBJECTCLAS,
                    OBJECTID = cd.OBJECTID,
                    CHANGENR = cd.CHANGENR,
                    CHANGE_IND = cd.CHANGE_IND,
                    TCODE = cd.TCODE,
                    UDATE = cd.UDATE,
                    UTIME = cd.UTIME,
                    UDATE_DT = cd.UDATE_DT,
                    CreationDateTime = now,
                    COMP_KEY = cd.COMP_KEY
                });
                adi.RowsInserted++;
            }

            db.tblAdminDataImport.Add(adi);
            db.SaveChanges();

            return list;
        }

        public static List<CDHDR> GetListCDHDR(string filename)
        {
            List<CDHDR> list = new List<CDHDR>();
            CsvFile file = new CsvFile(filename);
            DataTable dt = file.GetDataTable();
            string[] colNames = file.GetColumnNames();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                CDHDR nw = GetCDHDR(colNames, dr);
                cnt++;
                list.Add(nw);
            }
            return list;
        }

        public static CDHDR GetCDHDR(string[] colNames, DataRow dr)
        {
            CDHDR cd = new CDHDR();
            cd.COMP_KEY = dr[colNames[0]].ToString().Trim();
            cd.OBJECTCLAS = dr[colNames[1]].ToString().Trim();
            cd.OBJECTID = dr[colNames[2]].ToString().Trim();
            cd.CHANGENR = dr[colNames[3]].ToString().Trim();
            cd.UDATE = dr[colNames[4]].ToString().Trim();
            cd.UTIME = dr[colNames[5]].ToString().Trim();
            cd.TCODE = dr[colNames[6]].ToString().Trim();
            cd.CHANGE_IND = dr[colNames[7]].ToString().Trim();
            cd.UDATE_DT = ConvertSAPDateTime(cd.UDATE, cd.UTIME);
            return cd;
        }

        public static List<CDPOS> UpdateCDPOS(EKDBContext db, string filename)
        {
            DateTime now = DateTime.Now;
            List<CDPOS> list = GetListCDPOS(filename);

            AdminDataImport adi = new AdminDataImport();
            adi.ImportFileName = filename;
            adi.ImportDateTime = now;
            adi.TargetTable = "tblCDPOS";
            adi.RowsInFile = list.Count;


            foreach (CDPOS cd in list)
            {
                // Header
                CDHDR cdhdrDB = db.tblCDHDR.Where(c => c.CHANGENR == cd.CHANGENR).FirstOrDefault();
                if (cdhdrDB == null)
                {
                    throw new Exception("Änderungsbeleg " + cd.CHANGENR + " nicht vorhanden");
                }

                db.tblCDPOS.Add(new CDPOS()
                {
                    CDHDR_ID = cdhdrDB.ID,
                    OBJECTCLAS = cd.OBJECTCLAS,
                    OBJECTID = cd.OBJECTID,
                    CHANGENR = cd.CHANGENR,
                    CHNGIND = cd.CHNGIND,
                    UDATE_DT = cdhdrDB.UDATE_DT,
                    TABKEY = cd.TABKEY,
                    TABNAME = cd.TABNAME,
                    FNAME = cd.FNAME,
                    CUKY_NEW = cd.CUKY_NEW,
                    CUKY_OLD = cd.CUKY_OLD,
                    UNIT_NEW = cd.UNIT_NEW,
                    UNIT_OLD = cd.UNIT_OLD,
                    VALUE_NEW = cd.VALUE_NEW,
                    VALUE_OLD = cd.VALUE_OLD,
                    CreationDateTime = now,
                    COMP_KEY = cd.COMP_KEY
                });
                adi.RowsInserted++;
            }

            db.tblAdminDataImport.Add(adi);
            db.SaveChanges();

            return list;
        }
        public static List<CDPOS> GetListCDPOS(string filename)
        {
            List<CDPOS> list = new List<CDPOS>();
            CsvFile file = new CsvFile(filename);
            DataTable dt = file.GetDataTable();
            string[] colNames = file.GetColumnNames();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                CDPOS cd = GetCDPOS(colNames, dr);
                cnt++;
                list.Add(cd);
            }
            return list;
        }

        public static CDPOS GetCDPOS(string[] colNames, DataRow dr)
        {
            CDPOS cd = new CDPOS();
            cd.COMP_KEY = dr[colNames[0]].ToString().Trim();
            cd.OBJECTCLAS = dr[colNames[1]].ToString().Trim();
            cd.OBJECTID = dr[colNames[2]].ToString().Trim();
            cd.CHANGENR = dr[colNames[3]].ToString().Trim();
            cd.TABNAME = dr[colNames[4]].ToString().Trim();
            cd.TABKEY = dr[colNames[5]].ToString().Trim();
            cd.FNAME = dr[colNames[6]].ToString().Trim();
            cd.CHNGIND = dr[colNames[7]].ToString().Trim();
            cd.UNIT_OLD = dr[colNames[8]].ToString().Trim();
            cd.UNIT_NEW = dr[colNames[9]].ToString().Trim();
            cd.CUKY_OLD = dr[colNames[10]].ToString().Trim();
            cd.CUKY_NEW = dr[colNames[11]].ToString().Trim();
            cd.VALUE_NEW = dr[colNames[12]].ToString().Trim();
            cd.VALUE_OLD = dr[colNames[13]].ToString().Trim();
            cd.UDATE_DT = new DateTime();
            return cd;
        }

        public static List<EK_KRED> UpdateKRED(EKDBContext db, string filename)
        {
            DateTime now = DateTime.Now;
            List<EK_KRED> list = GetListKRED(filename);
            AdminDataImport adi = new AdminDataImport();
            adi.ImportFileName = filename;
            adi.ImportDateTime = now;
            adi.TargetTable = "tblKRED";
            adi.RowsInFile = list.Count;

            var listDB = db.Set<EK_KRED>();
            foreach (EK_KRED kred in list)
            {
                EK_KRED kredDB = listDB.Where(k => k.LIFNR == kred.LIFNR).FirstOrDefault();
                if (kredDB == null)
                {
                    db.tblKRED.Add(new EK_KRED()
                    {
                        LIFNR = kred.LIFNR,
                        LAND1 = kred.LAND1,
                        NAME1 = kred.NAME1,
                        NAME2 = kred.NAME2,
                        NAME3 = kred.NAME3,
                        NAME4 = kred.NAME4,
                        ORT01 = kred.ORT01,
                        ORT02 = kred.ORT02,
                        PFACH = kred.PFACH,
                        PSTL2 = kred.PSTL2,
                        PSTLZ = kred.PSTLZ,
                        REGIO = kred.REGIO,
                        SORTL = kred.SORTL,
                        STRAS = kred.STRAS,
                        KTOKK = kred.KTOKK,
                        LFURL = kred.LFURL,
                        LOEVM = kred.LOEVM,
                        SPERR = kred.SPERR,
                        SPERM = kred.SPERM,
                        COMP_KEY = kred.COMP_KEY,
                        CreationDateTime = now
                    });
                    adi.RowsInserted++;
                }
                else
                {
                    if (!kredDB.Equals(kred))
                    {
                        // alte Daten in Versionstabelle sichern
                        db.tblKRED_V.Add(new EK_KRED_V(kredDB));
                        // neue Daten
                        kredDB.LAND1 = kred.LAND1;
                        kredDB.NAME1 = kred.NAME1;
                        kredDB.NAME2 = kred.NAME2;
                        kredDB.NAME3 = kred.NAME3;
                        kredDB.NAME4 = kred.NAME4;
                        kredDB.ORT01 = kred.ORT01;
                        kredDB.ORT02 = kred.ORT02;
                        kredDB.PFACH = kred.PFACH;
                        kredDB.PSTL2 = kred.PSTL2;
                        kredDB.PSTLZ = kred.PSTLZ;
                        kredDB.REGIO = kred.REGIO;
                        kredDB.SORTL = kred.SORTL;
                        kredDB.STRAS = kred.STRAS;
                        kredDB.KTOKK = kred.KTOKK;
                        kredDB.LFURL = kred.LFURL;
                        kredDB.LOEVM = kred.LOEVM;
                        kredDB.SPERR = kred.SPERR;
                        kredDB.SPERM = kred.SPERM;
                        kredDB.UpdateDateTime = now;
                        db.tblKRED.Update(kredDB);
                        adi.RowsUpdated++;
                    }
                }
            }

            db.tblAdminDataImport.Add(adi);
            db.SaveChanges();


            return list;
        }
        public static List<EK_KRED> GetListKRED(string filename)
        {
            List<EK_KRED> list = new List<EK_KRED>();
            CsvFile file = new CsvFile(filename);
            DataTable dt = file.GetDataTable();
            string[] colNames = file.GetColumnNames();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                EK_KRED kred = GetKRED(colNames, dr);
                cnt++;
                list.Add(kred);
            }
            return list;
        }
        public static EK_KRED GetKRED(string[] colNames, DataRow dr)
        {
            EK_KRED kred = new EK_KRED();
            kred.COMP_KEY = dr[colNames[0]].ToString().Trim();
            kred.LIFNR = dr[colNames[1]].ToString().Trim();
            kred.LAND1 = dr[colNames[2]].ToString().Trim();
            kred.NAME1 = dr[colNames[3]].ToString().Trim();
            kred.NAME2 = dr[colNames[4]].ToString().Trim();
            kred.NAME3 = dr[colNames[5]].ToString().Trim();
            kred.NAME4 = dr[colNames[6]].ToString().Trim();
            kred.ORT01 = dr[colNames[7]].ToString().Trim();
            kred.ORT02 = dr[colNames[8]].ToString().Trim();
            kred.PFACH = dr[colNames[9]].ToString().Trim();
            kred.PSTL2 = dr[colNames[10]].ToString().Trim();
            kred.PSTLZ = dr[colNames[11]].ToString().Trim();
            kred.REGIO = dr[colNames[12]].ToString().Trim();
            kred.SORTL = dr[colNames[13]].ToString().Trim();
            kred.STRAS = dr[colNames[14]].ToString().Trim();
            kred.KTOKK = dr[colNames[15]].ToString().Trim();
            kred.LFURL = dr[colNames[16]].ToString().Trim();
            kred.LOEVM = dr[colNames[17]].ToString().Trim().Equals("X");
            kred.SPERR = dr[colNames[18]].ToString().Trim().Equals("X");
            kred.SPERM = dr[colNames[19]].ToString().Trim().Equals("X");
            return kred;
        }

        public static List<RBKP> UpdateRBKP(EKDBContext db, string filename)
        {
            DateTime now = DateTime.Now;
            List<RBKP> list = GetListRBKP(filename);
            AdminDataImport adi = new AdminDataImport();
            adi.ImportFileName = filename;
            adi.ImportDateTime = now;
            adi.TargetTable = "tblRBKP";
            adi.RowsInFile = list.Count;

            foreach (RBKP rbkp in list)
            {
                RBKP rbkpDB = db.tblRBKP.Where(e => e.COMP_KEY == rbkp.COMP_KEY).FirstOrDefault();

                if (rbkpDB == null || (rbkpDB != null && !rbkpDB.Equals(rbkp)))
                {
                    EK_BUKRS bukDB = db.tblBUKRS.Where(b => b.BUKRS == rbkp.BUKRS).FirstOrDefault();
                    if (bukDB == null)
                    {
                        throw new Exception("Buchungskreis " + rbkp.BUKRS + " nicht vorhanden");
                    }

                    EK_KRED kredDB = db.tblKRED.Where(k => k.LIFNR == rbkp.LIFNR).FirstOrDefault();
                    if (kredDB == null)
                    {
                        throw new Exception("Kreditor " + rbkp.LIFNR + " nicht vorhanden");
                    }

                   

                    if (rbkpDB == null)
                    {
                        db.tblRBKP.Add(new RBKP()
                        {
                            COMP_KEY = rbkp.COMP_KEY,
                            BELNR = rbkp.BELNR,
                            GJAHR = rbkp.GJAHR,
                            BLART = rbkp.BLART,
                            BLDAT = rbkp.BLDAT,
                            BLDAT_DT = rbkp.BLDAT_DT,
                            BUDAT = rbkp.BUDAT,
                            BUDAT_DT = rbkp.BUDAT_DT,
                            USNAM = rbkp.USNAM,
                            CPUDT = rbkp.CPUDT,
                            CPUDT_DT = rbkp.CPUDT_DT,
                            CPUTM = rbkp.CPUTM,
                            VGART = rbkp.VGART,
                            BUKRS = rbkp.BUKRS,
                            BUKRS_ID = bukDB.ID,
                            LIFNR = rbkp.LIFNR,
                            LIFNR_ID = kredDB.ID,
                            WAERS = rbkp.WAERS,
                            RMWWR = rbkp.RMWWR,
                            BEZNK = rbkp.BEZNK,
                            WMWST1 = rbkp.WMWST1,
                            MWSKZ1 = rbkp.MWSKZ1,
                            ZTERM = rbkp.ZTERM,
                            ZBD1T = rbkp.ZBD1T,
                            ZBD1P = rbkp.ZBD1P,
                            ZBD2T = rbkp.ZBD2T,
                            ZBD2P = rbkp.ZBD2P,
                            ZBD3T = rbkp.ZBD3T,
                            WSKTO = rbkp.WSKTO,
                            XRECH = rbkp.XRECH,
                            XMWST = rbkp.XMWST,
                            STBLG = rbkp.STBLG,
                            STJAH = rbkp.STJAH,
                            MWSKZ_BNK = rbkp.MWSKZ_BNK,
                            IVTYP = rbkp.IVTYP,
                            XRBTX = rbkp.XRBTX,
                            REPART = rbkp.REPART,
                            RBSTAT = rbkp.RBSTAT,
                            ARKUEN = rbkp.ARKUEN,
                            ARKUEMW = rbkp.ARKUEMW,
                            MAKZN = rbkp.MAKZN,
                            MAKZMW = rbkp.MAKZMW,
                            XAUTAKZ = rbkp.XAUTAKZ,
                            LANDL = rbkp.LANDL,
                            LZBKZ = rbkp.LZBKZ,
                            EMPFB = rbkp.EMPFB,
                            BVTYP = rbkp.BVTYP,
                            HBKID = rbkp.HBKID,
                            ZUONR = rbkp.ZUONR,
                            ZLSPR = rbkp.ZLSPR,
                            ZLSCH = rbkp.ZLSCH,
                            ZFBDT = rbkp.ZFBDT,
                            ZFBDT_DT = rbkp.ZFBDT_DT,
                            REBZG = rbkp.REBZG,
                            REBZJ = rbkp.REBZJ,
                            ZBFIX = rbkp.ZBFIX,
                            FRGKZ = rbkp.FRGKZ,
                            GSBER = rbkp.GSBER,
                            CreationDateTime = now
                        });
                        adi.RowsInserted++;
                    }
                    else
                    {
                        if (!rbkpDB.Equals(rbkp))
                        {
                            rbkpDB.COMP_KEY = rbkp.COMP_KEY;
                            rbkpDB.BELNR = rbkp.BELNR;
                            rbkpDB.GJAHR = rbkp.GJAHR;
                            rbkpDB.BLART = rbkp.BLART;
                            rbkpDB.BLDAT = rbkp.BLDAT;
                            rbkpDB.BUDAT = rbkp.BUDAT;
                            rbkpDB.USNAM = rbkp.USNAM;
                            rbkpDB.CPUDT = rbkp.CPUDT;
                            rbkpDB.CPUTM = rbkp.CPUTM;
                            rbkpDB.VGART = rbkp.VGART;
                            rbkpDB.BUKRS = rbkp.BUKRS;
                            rbkpDB.BUKRS_ID = bukDB.ID;
                            rbkpDB.LIFNR = rbkp.LIFNR;
                            rbkpDB.LIFNR_ID = kredDB.ID;
                            rbkpDB.WAERS = rbkp.WAERS;
                            rbkpDB.RMWWR = rbkp.RMWWR;
                            rbkpDB.BEZNK = rbkp.BEZNK;
                            rbkpDB.WMWST1 = rbkp.WMWST1;
                            rbkpDB.MWSKZ1 = rbkp.MWSKZ1;
                            rbkpDB.ZTERM = rbkp.ZTERM;
                            rbkpDB.ZBD1T = rbkp.ZBD1T;
                            rbkpDB.ZBD1P = rbkp.ZBD1P;
                            rbkpDB.ZBD2T = rbkp.ZBD2T;
                            rbkpDB.ZBD2P = rbkp.ZBD2P;
                            rbkpDB.ZBD3T = rbkp.ZBD3T;
                            rbkpDB.WSKTO = rbkp.WSKTO;
                            rbkpDB.XRECH = rbkp.XRECH;
                            rbkpDB.XMWST = rbkp.XMWST;
                            rbkpDB.STBLG = rbkp.STBLG;
                            rbkpDB.STJAH = rbkp.STJAH;
                            rbkpDB.MWSKZ_BNK = rbkp.MWSKZ_BNK;
                            rbkpDB.IVTYP = rbkp.IVTYP;
                            rbkpDB.XRBTX = rbkp.XRBTX;
                            rbkpDB.REPART = rbkp.REPART;
                            rbkpDB.RBSTAT = rbkp.RBSTAT;
                            rbkpDB.ARKUEN = rbkp.ARKUEN;
                            rbkpDB.ARKUEMW = rbkp.ARKUEMW;
                            rbkpDB.MAKZN = rbkp.MAKZN;
                            rbkpDB.MAKZMW = rbkp.MAKZMW;
                            rbkpDB.XAUTAKZ = rbkp.XAUTAKZ;
                            rbkpDB.LANDL = rbkp.LANDL;
                            rbkpDB.LZBKZ = rbkp.LZBKZ;
                            rbkpDB.EMPFB = rbkp.EMPFB;
                            rbkpDB.BVTYP = rbkp.BVTYP;
                            rbkpDB.HBKID = rbkp.HBKID;
                            rbkpDB.ZUONR = rbkp.ZUONR;
                            rbkpDB.ZLSPR = rbkp.ZLSPR;
                            rbkpDB.ZLSCH = rbkp.ZLSCH;
                            rbkpDB.ZFBDT = rbkp.ZFBDT;
                            rbkpDB.REBZG = rbkp.REBZG;
                            rbkpDB.REBZJ = rbkp.REBZJ;
                            rbkpDB.ZBFIX = rbkp.ZBFIX;
                            rbkpDB.FRGKZ = rbkp.FRGKZ;
                            rbkpDB.GSBER = rbkp.GSBER;
                            rbkpDB.UpdateDateTime = DateTime.Now;
                            db.tblRBKP.Update(rbkpDB);
                            adi.RowsUpdated++;
                        }
                    }

                }
            }
            db.tblAdminDataImport.Add(adi);
            db.SaveChanges();

            return list;
        }
        public static List<RBKP> GetListRBKP(string filename)
        {
            List<RBKP> list = new List<RBKP>();
            CsvFile file = new CsvFile(filename);
            DataTable dt = file.GetDataTable();
            string[] colNames = file.GetColumnNames();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                RBKP rbkp = GetRBKP(colNames, dr);
                cnt++;
                if (rbkp.COMP_KEY.Length > 0)
                {
                    list.Add(rbkp);
                }
            }
            return list;
        }
        public static RBKP GetRBKP(string[] colNames, DataRow dr)
        {
            RBKP rbkp = new RBKP();
            rbkp.BELNR = dr[colNames[0]].ToString().Trim();
            rbkp.GJAHR = dr[colNames[1]].ToString().Trim();
            rbkp.BLART = dr[colNames[2]].ToString().Trim();
            rbkp.BLDAT = dr[colNames[3]].ToString().Trim();
            rbkp.BLDAT_DT = ConvertSAPDate(rbkp.BLDAT);
            rbkp.BUDAT = dr[colNames[4]].ToString().Trim();
            rbkp.BUDAT_DT = ConvertSAPDate(rbkp.BUDAT);
            rbkp.USNAM = dr[colNames[5]].ToString().Trim();
            rbkp.CPUDT = dr[colNames[6]].ToString().Trim();
            rbkp.CPUDT_DT = ConvertSAPDate(rbkp.CPUDT);
            rbkp.CPUTM = dr[colNames[7]].ToString().Trim();
            rbkp.VGART = dr[colNames[8]].ToString().Trim();
            rbkp.BUKRS = dr[colNames[9]].ToString().Trim();            
            rbkp.LIFNR = dr[colNames[10]].ToString().Trim();            
            rbkp.WAERS = dr[colNames[11]].ToString().Trim();
            rbkp.RMWWR = Decimal.Parse(dr[colNames[12]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.BEZNK = Decimal.Parse(dr[colNames[13]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.WMWST1 =Decimal.Parse(dr[colNames[14]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.MWSKZ1 = dr[colNames[15]].ToString().Trim();
            rbkp.ZTERM = dr[colNames[16]].ToString().Trim();
            rbkp.ZBD1T = Int32.Parse(dr[colNames[17]].ToString().Trim());
            rbkp.ZBD1P = Decimal.Parse(dr[colNames[18]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.ZBD2T = Int32.Parse(dr[colNames[19]].ToString().Trim());
            rbkp.ZBD2P = Decimal.Parse(dr[colNames[20]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.ZBD3T = Int32.Parse(dr[colNames[21]].ToString().Trim());
            rbkp.WSKTO = Decimal.Parse(dr[colNames[22]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.XRECH = dr[colNames[23]].ToString().Trim();
            rbkp.XMWST = dr[colNames[24]].ToString().Trim();
            rbkp.STBLG = dr[colNames[25]].ToString().Trim();
            rbkp.STJAH = dr[colNames[26]].ToString().Trim();
            rbkp.MWSKZ_BNK = dr[colNames[27]].ToString().Trim();
            rbkp.IVTYP = dr[colNames[28]].ToString().Trim();
            rbkp.XRBTX = dr[colNames[29]].ToString().Trim();
            rbkp.REPART = dr[colNames[30]].ToString().Trim();
            rbkp.RBSTAT = dr[colNames[31]].ToString().Trim();
            rbkp.ARKUEN = Decimal.Parse(dr[colNames[32]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.ARKUEMW = Decimal.Parse(dr[colNames[33]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.MAKZN = Decimal.Parse(dr[colNames[34]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.MAKZMW = Decimal.Parse(dr[colNames[35]].ToString().Trim(), CultureInfo.InvariantCulture);
            rbkp.XAUTAKZ = dr[colNames[36]].ToString().Trim();
            rbkp.LANDL = dr[colNames[37]].ToString().Trim();
            rbkp.LZBKZ = dr[colNames[38]].ToString().Trim();
            rbkp.EMPFB = dr[colNames[39]].ToString().Trim();
            rbkp.BVTYP = dr[colNames[40]].ToString().Trim();
            rbkp.HBKID = dr[colNames[41]].ToString().Trim();
            rbkp.ZUONR = dr[colNames[42]].ToString().Trim();
            rbkp.ZLSPR = dr[colNames[43]].ToString().Trim();
            rbkp.ZLSCH = dr[colNames[44]].ToString().Trim();
            rbkp.ZFBDT = dr[colNames[45]].ToString().Trim();
            rbkp.ZFBDT_DT = ConvertSAPDate(rbkp.ZFBDT);
            rbkp.REBZG = dr[colNames[46]].ToString().Trim();
            rbkp.REBZJ = dr[colNames[47]].ToString().Trim();
            rbkp.ZBFIX = dr[colNames[48]].ToString().Trim();
            rbkp.FRGKZ = dr[colNames[49]].ToString().Trim();
            rbkp.GSBER = dr[colNames[50]].ToString().Trim();
            rbkp.COMP_KEY = dr[colNames[51]].ToString().Trim();
            return rbkp;
        }


    }
}
