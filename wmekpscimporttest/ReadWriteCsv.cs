﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

// ****************************************************************************************
// Source:
// http://www.codeproject.com/Articles/415732/Reading-and-Writing-CSV-Files-in-Csharp
//
// ****************************************************************************************

namespace ReadWriteCsv
{
    /// <summary>
    /// Class to store one CSV row
    /// </summary>
    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }

    /// <summary>
    /// Class to write data to a CSV file
    /// </summary>
    public class CsvFileWriter : StreamWriter
    {
        private static readonly char sep = ';';

        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            StringBuilder builder = new StringBuilder();
            bool firstColumn = true;
            foreach (string value in row)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    builder.Append(sep);
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', sep }) != -1)
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                    builder.Append(value);
                firstColumn = false;
            }
            row.LineText = builder.ToString();
            WriteLine(row.LineText);
        }
    }

    /// <summary>
    /// Class to read data from a CSV file
    /// </summary>
    public class CsvFileReader : StreamReader
    {
        private static readonly char sep = ';';

        public CsvFileReader(Stream stream)
            : base(stream)
        {
        }

        public CsvFileReader(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Reads a row of data from a CSV file
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool ReadRow(CsvRow row)
        {
            row.LineText = ReadLine();
            if (String.IsNullOrEmpty(row.LineText))
                return false;

            int pos = 0;
            int rows = 0;

            while (pos < row.LineText.Length)
            {
                string value;

                // Special handling for quoted field
                if (row.LineText[pos] == '"')
                {
                    // Skip initial quote
                    pos++;

                    // Parse quoted value
                    int start = pos;
                    while (pos < row.LineText.Length)
                    {
                        // Test for quote character
                        if (row.LineText[pos] == '"')
                        {
                            // Found one
                            pos++;

                            // If two quotes together, keep one
                            // Otherwise, indicates end of value
                            if (pos >= row.LineText.Length || row.LineText[pos] != '"')
                            {
                                pos--;
                                break;
                            }
                        }
                        pos++;
                    }
                    value = row.LineText.Substring(start, pos - start);
                    value = value.Replace("\"\"", "\"");
                }
                else
                {
                    // Parse unquoted value
                    int start = pos;
                    while (pos < row.LineText.Length && row.LineText[pos] != sep)
                        pos++;
                    value = row.LineText.Substring(start, pos - start);
                }

                // Add field to list
                if (rows < row.Count)
                    row[rows] = value;
                else
                    row.Add(value);
                rows++;

                // Eat up to and including next comma
                while (pos < row.LineText.Length && row.LineText[pos] != sep)
                    pos++;
                if (pos < row.LineText.Length)
                    pos++;
            }
            // Delete any unused items
            while (row.Count > rows)
                row.RemoveAt(rows);

            // Return true if any columns read
            return (row.Count > 0);
        }
    }


    public class CsvFile
    {
        private DataTable dt { set; get; }
        private string[] colNames { set; get; }

        public CsvFile()
        {
            dt = new DataTable();
            colNames = new string[100];
        }
        public CsvFile(string filename)
        {
            dt = new DataTable();
            colNames = new string[100];

            int cnt = 0, j = 0;
            try
            {
                using (CsvFileReader reader = new CsvFileReader(filename))
                {
                    CsvRow row = new CsvRow();
                    while (reader.ReadRow(row))
                    {
                        if (cnt == 0)
                        {
                            foreach (string s in row)
                            {
                                if (s != null && s.Length > 0)
                                {
                                    dt.Columns.Add(s, typeof(string));
                                    colNames[j] = s;
                                    j++;
                                }
                            }
                            cnt++;
                        }
                        else
                        {
                            j = 0;
                            DataRow dr = dt.NewRow();
                            foreach (string s in row)
                            {
                                if (colNames[j] != null)
                                {
                                    dr[colNames[j]] = s;
                                    j++;
                                }
                            }
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public DataTable GetDataTable()
        {
            return dt;
        }

        public string[] GetColumnNames()
        {
            int cntColumns = 0;
            for (int i = 0; i < 100; i++)
            {
                if (colNames[i] != null)
                {
                    cntColumns++;
                }
            }

            string[] colnames = new string[cntColumns];
            for (int i = 0; i < cntColumns; i++)
            {
                colnames[i] = colNames[i];
            }
            return colnames;
        }

    }
}